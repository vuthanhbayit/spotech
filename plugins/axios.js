import axios from 'axios';

export default function ({store, app, req}) {
  /*axios.interceptors.request.use(function (config) {
    let token = store.getters.accessToken;
    let locale = store.getters.getLocale
    let currency = store.getters.getCurrency

    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    }

    config.headers['Language-Code'] = locale;
    config.headers['Currency-Code'] = currency;
    config.headers['X-Request-Time'] = (new Date()).getTime();
    config.headers['Content-Type'] = 'application/json; charset=utf-8';
    config.headers['X-Requested-Env'] = 'web';
    config.headers['Accept'] = "application/json";
    return config;
  }, function (err) {
    return Promise.reject(err);
  });*/
  
	let token = store.getters.accessToken;
	let locale = store.getters.getLocale
	let currency = store.getters.getCurrency

	if( token ) {
			axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
	}
	axios.defaults.headers.common['Language-Code'] = locale;
	axios.defaults.headers.common['Currency-Code'] = currency;
	axios.defaults.headers.common['X-Request-Time'] = ( new Date() ).getTime();
	axios.defaults.headers.common['Content-Type'] = 'application/json; charset=utf-8';
	axios.defaults.headers.common['X-Requested-Env'] = 'web';
	
}
