import axios from "~/services/request";
import cookie from 'js-cookie'

const expire = { expires: 360 };

export default async function ({app, store}) {

  let lang = cookie.get('lang');
  let currency = cookie.get('currency');
  let unit = cookie.get('unit');
  if( ! lang || ! currency )
  {
      let location = null;
      try{
          location = JSON.parse(cookie.get('location'));
      }catch(e){}
      if( ! location )
      {
          /*location = await axios.get('/getLocationMember');
          let data = location.data || null;
          if (data != null)
          {
              cookie.set('location', JSON.stringify(location), expire );
              lang = data.language_code || null;
              currency = data.currency_code || null;
              unit = data.unit || null;
          }*/
      }
  }
  if( ! lang ){ lang = 'en'; }
  if( ! currency ){ currency = 'USD';}
  if( ! unit ){ unit = '$'; }

  app.i18n.setLocaleCookie(lang);
  //app.switchLocalePath(lang)
  store.commit('SET_LANG', lang);
  cookie.set('lang', lang, expire);

  store.commit('SET_CURRENCY', currency);
  cookie.set('currency', currency, expire);

  cookie.set('unit', unit, expire);
  store.commit('SET_CURRENCY_UNIT', unit);
}
