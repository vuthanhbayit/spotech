import Vue from 'vue';

export default function () {
    Vue.mixin({
        data() {
            return {
                __notify: null
            }
        },
        methods: {
            $error(message = 'Successful') {
                this.$notify.error({
                    title: this.$t('Error'),
                    message: message
                })
            },
            $success(message = 'Error') {
                this.$notify.success({
                    title: this.$t('Success'),
                    message: message
                })
            },
            showNotification(title, message, type, multi = false) {
                if( this.__notify != null && !multi ) {
                    this.__notify.close();
                }
                this.__notify = this.$notify({
                    title: title,
                    message: message,
                    type: type
                });
            }
        }
    })
}
