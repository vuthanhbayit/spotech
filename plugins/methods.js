import Vue from 'vue'

export default function ({store}) {
  Vue.mixin({
    methods: {
      $formatDate: (value, format) => {
        if (format) {
          return '1' + value;
        }
        return value
      },
      async $translate(id, name) {
        let obj = {
          object_id: id,
          object_name: name,
          language_code: store.getters['getLocale']
        }
        return await store.dispatch('members/viewProfile/getExpertTranslation', obj)
      },
      featuresDevelopment() {
        this.$confirm(this.$t('FeaturesAreUnderDevelopment'), this.$t('Info'), {
          confirmButtonText: 'OK',
          showConfirmButton: false,
          showCancelButton: false,
          type: 'info',
        })
      },
      $currentYear() {
        let year = new Date();
        return year.getFullYear();
      },
    }
  })
}
