import Cookie from 'js-cookie'

const REFER_CODE = 'refer_code';

export function getUserInfor() {
  return Cookie.getJSON('member');
}

export function setUserInfor(infor) {
  return Cookie.set('member', infor);
}

export function setCompany(company) {
  Cookie.set('company', '');
  return Cookie.set('company', company);
}

export function removeInfor() {
  return Cookie.remove('member');
}

export function setReferCode(code) {
  Cookie.set(REFER_CODE, '');
  return Cookie.set(REFER_CODE, code);
}

export function getReferCode() {
  return Cookie.get(REFER_CODE);
}


