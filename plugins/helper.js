import Vue from 'vue'
//import config from '../config'
import _ from 'lodash'
import https from 'https'
Vue.prototype.currentYear = () => {
  let year = new Date();
  return year.getFullYear();
}
/*
Vue.prototype.year = () => {
  let max = new Date().getFullYear();
  return Array.from(Array(config.min_year_graduation), (x, index) => max - index);
}
*/

Vue.prototype.sevenday = () => {
  return Array.from(Array(7), (x, index) => index);
}

Vue.prototype.$arrayFromToNumber = (from, to) => {
  return Array.from(Array(to - from + 1), (v, k) => k + from)
}

Vue.prototype.assignQueryNotNull = (listQuery) => {
  const query = {};
  _.forEach(listQuery, (q, i) => {
    if (_.isArray(q)) q = JSON.stringify(q);
    if ( q === 0 ) {
      query[i] = 0
    } else {
      (!q && _.isEmpty(q)) || (query[i] = q);
    }
  });
  return query;
}

Vue.prototype.getCurrencyUnit = (locale) => {
  const baseUnit = {vi: 'VND', ja: 'JPY', en: 'USD'};
  return baseUnit[locale];
}

Vue.prototype.convertCurrency = (amount, fromCurrency, toCurrency, callback) => {
  const apiKey = 'ab06b02a9808405522d3';
  const query = fromCurrency + '_' + toCurrency;
  const url = `https://free.currconv.com/api/v7/convert?q=${query}&compact=ultra&apiKey=${apiKey}`;
  https.get(url, function(res){
    let body = '';
    res.on('data', (chunk) => { body += chunk });
    res.on('end', function(){
      try {
        const val = JSON.parse(body)[query];
        return val ? callback(null, Math.round(val * amount * 100) / 100)
          : callback(new Error("Value not found for " + query));
      } catch(e) {
        return callback(e);
      }
    });
  }).on('error', function(e){
    callback(e);
  });
}

Vue.prototype.vndToUsd = (amount, fromCurrency, toCurrency) => {
  return new Promise( (resolve, reject) => {
    const apiKey = 'ab06b02a9808405522d3';
    const query = fromCurrency + '_' + toCurrency;
    const url = `https://free.currconv.com/api/v7/convert?q=${query}&compact=ultra&apiKey=${apiKey}`;
    https.get(url, function(res){
      let body = '';
      res.on('data', (chunk) => { body += chunk });
      res.on('end', function(){
        try {
          const val = JSON.parse(body)[query];
          return val ? resolve({error: null, amount: Math.round(val * amount * 100) / 100})
            : reject(new Error("Value not found for " + query));
        } catch(e) {
          return reject(e);
        }
      });
    }).on('error', function(e){
      reject(e);
    });
  });
  
}

Vue.prototype.getCurencySymbol = (code) => {
var symbols = {
  'AED': 'د.إ',
  'AFN': '؋',
  'ALL': 'L',
  'AMD': '֏',
  'ANG': 'ƒ',
  'AOA': 'Kz',
  'ARS': '$',
  'AUD': '$',
  'AWG': 'ƒ',
  'AZN': '₼',
  'BAM': 'KM',
  'BBD': '$',
  'BDT': '৳',
  'BGN': 'лв',
  'BHD': '.د.ب',
  'BIF': 'FBu',
  'BMD': '$',
  'BND': '$',
  'BOB': '$b',
  'BRL': 'R$',
  'BSD': '$',
  'BTC': '฿',
  'BTN': 'Nu.',
  'BWP': 'P',
  'BYR': 'Br',
  'BYN': 'Br',
  'BZD': 'BZ$',
  'CAD': '$',
  'CDF': 'FC',
  'CHF': 'CHF',
  'CLP': '$',
  'CNY': '¥',
  'COP': '$',
  'CRC': '₡',
  'CUC': '$',
  'CUP': '₱',
  'CVE': '$',
  'CZK': 'Kč',
  'DJF': 'Fdj',
  'DKK': 'kr',
  'DOP': 'RD$',
  'DZD': 'دج',
  'EEK': 'kr',
  'EGP': '£',
  'ERN': 'Nfk',
  'ETB': 'Br',
  'ETH': 'Ξ',
  'EUR': '€',
  'FJD': '$',
  'FKP': '£',
  'GBP': '£',
  'GEL': '₾',
  'GGP': '£',
  'GHC': '₵',
  'GHS': 'GH₵',
  'GIP': '£',
  'GMD': 'D',
  'GNF': 'FG',
  'GTQ': 'Q',
  'GYD': '$',
  'HKD': '$',
  'HNL': 'L',
  'HRK': 'kn',
  'HTG': 'G',
  'HUF': 'Ft',
  'IDR': 'Rp',
  'ILS': '₪',
  'IMP': '£',
  'INR': '₹',
  'IQD': 'ع.د',
  'IRR': '﷼',
  'ISK': 'kr',
  'JEP': '£',
  'JMD': 'J$',
  'JOD': 'JD',
  'JPY': '¥',
  'KES': 'KSh',
  'KGS': 'лв',
  'KHR': '៛',
  'KMF': 'CF',
  'KPW': '₩',
  'KRW': '₩',
  'KWD': 'KD',
  'KYD': '$',
  'KZT': 'лв',
  'LAK': '₭',
  'LBP': '£',
  'LKR': '₨',
  'LRD': '$',
  'LSL': 'M',
  'LTC': 'Ł',
  'LTL': 'Lt',
  'LVL': 'Ls',
  'LYD': 'LD',
  'MAD': 'MAD',
  'MDL': 'lei',
  'MGA': 'Ar',
  'MKD': 'ден',
  'MMK': 'K',
  'MNT': '₮',
  'MOP': 'MOP$',
  'MRO': 'UM',
  'MRU': 'UM',
  'MUR': '₨',
  'MVR': 'Rf',
  'MWK': 'MK',
  'MXN': '$',
  'MYR': 'RM',
  'MZN': 'MT',
  'NAD': '$',
  'NGN': '₦',
  'NIO': 'C$',
  'NOK': 'kr',
  'NPR': '₨',
  'NZD': '$',
  'OMR': '﷼',
  'PAB': 'B/.',
  'PEN': 'S/.',
  'PGK': 'K',
  'PHP': '₱',
  'PKR': '₨',
  'PLN': 'zł',
  'PYG': 'Gs',
  'QAR': '﷼',
  'RMB': '￥',
  'RON': 'lei',
  'RSD': 'Дин.',
  'RUB': '₽',
  'RWF': 'R₣',
  'SAR': '﷼',
  'SBD': '$',
  'SCR': '₨',
  'SDG': 'ج.س.',
  'SEK': 'kr',
  'SGD': '$',
  'SHP': '£',
  'SLL': 'Le',
  'SOS': 'S',
  'SRD': '$',
  'SSP': '£',
  'STD': 'Db',
  'STN': 'Db',
  'SVC': '$',
  'SYP': '£',
  'SZL': 'E',
  'THB': '฿',
  'TJS': 'SM',
  'TMT': 'T',
  'TND': 'د.ت',
  'TOP': 'T$',
  'TRL': '₤',
  'TRY': '₺',
  'TTD': 'TT$',
  'TVD': '$',
  'TWD': 'NT$',
  'TZS': 'TSh',
  'UAH': '₴',
  'UGX': 'USh',
  'USD': '$',
  'UYU': '$U',
  'UZS': 'лв',
  'VEF': 'Bs',
  'VND': '₫',
  'VUV': 'VT',
  'WST': 'WS$',
  'XAF': 'FCFA',
  'XBT': 'Ƀ',
  'XCD': '$',
  'XOF': 'CFA',
  'XPF': '₣',
  'YER': '﷼',
  'ZAR': 'R',
  'ZWD': 'Z$'
  };
  return symbols[code] ? symbols[code] : '';
}
