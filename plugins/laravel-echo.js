import Echo from "laravel-echo";
import config from '~/config';
import eventBus from '~/services/bus'
window.Pusher = require('pusher-js');

//Pusher.logToConsole = true

export default function ({store, app})
{
    let conf = config.socket;

    conf.auth = {
        headers: {
            'Authorization': 'Bearer ' + store.getters.accessToken,
            'X-Request-Time': ( new Date() ).getTime()
        }
    }

    window.Echo = new Echo(conf);

    if( process.browser && store.getters.isAuthenticated )
    {
        window.Echo.private('user-' + store.getters.getUserId)
            .listen('.message', (e) => {
                store.commit('SET_LAST_MESSAGE_EVENT', e);
                eventBus.$emit('onNewMessage', e);
            })
            .listen('.discussion', (e) => {
                eventBus.$emit('onNewDiscussion', e);
            })
            .listen('.discussion_update', (e) => {
                eventBus.$emit('onUpdateDiscussion', e);
            })
            .listen('.oauth_logout', (e) => {
                let tokens = e.tokens;
                if( tokens && tokens.length > 0 && tokens.indexOf(store.getters.accessToken) >= 0 ) {
                    //logout cause token revoke
                    window.location.href = '/app/logout'
                }
            })
            .listen('.notification', (e) => {
                eventBus.$emit('onNewNotification', e);
            });
    }
}
