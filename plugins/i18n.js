export default async function ({ app }) {
    app.i18n.onLanguageSwitched = (oldLocale, newLocale) => {
        app.$moment.locale(newLocale);
    };
}