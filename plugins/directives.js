import Vue from 'vue'

Vue.directive('focus', el => {
    el.focus();
})
