import cookie from 'js-cookie'

export default function ({store, app}) {
  const company = app.$cookies.get('company');
  const referCode = app.$cookies.get('refer_code');

  if (company) {
    store.commit('SET_COMPANY', company);
  }
  if (referCode) {
    store.commit('SET_REFER_CODE', referCode);
  }

  if (typeof (Storage) !== "undefined") {
    let memberType = JSON.parse(localStorage.getItem('memberType'));
    store.commit('members/profile/CHANGE_MEMBER_TYPE', memberType);
  }
}
