import Vue from 'vue';

export default function (context) {
    Vue.mixin({
        beforeCreate() {
            let locale = context.store.getters.getLocale
            context.app.$moment.locale( locale );
        }
    })
}
