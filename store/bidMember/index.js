import axios from "~/services/request";

export const actions = {
    async saveBidMembers({ commit }, opts) {
        return await axios.post('/members/saveBidMembers', opts);
    },
    async getBidMembers({ commit }, opts) {
        return await axios.get('/members/getBidMembers', {
            params: opts
        });
    },
    async getBidMembersTranslation({ commit }, opts) {
        return await axios.get('/members/getBidMembersTranslation', {
            params: opts
        });
    }
}
