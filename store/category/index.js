import axios from "~/services/request";

export const actions = {
    async search({ commit }, opts) {
        return await axios.post('/categories/search', opts);
    },
}
