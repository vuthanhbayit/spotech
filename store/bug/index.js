import axios from "~/services/request";

export const state = () => ({})
export const mutations = {}
export const getters = {}
export const actions = {
    async addBug({ commit }, opts) {
        return await axios.post('/bug/addBug', opts);
    },
    async getBugOptions({ commit }, opts) {
        return await axios.get('/bug/getBugOptions', {
            params: opts
        });
    },
}
