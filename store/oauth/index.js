import { getLoginLog, revokeToken } from "~/services/accountSettingApi";

export const state = () => ({})
export const mutations = {}
export const getters = {}
export const actions = {
    async getOauthLog(context, opts) {
        return await getLoginLog(opts);
    },
    async revokeToken(context, opts) {
        return await revokeToken(opts);
    },
}
