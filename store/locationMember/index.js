import axios from "~/services/request";

export const actions = {
    async getLocationMember({ commit }) {
        return await axios.get('/getLocationMember');
    },
}
