import axios from "~/services/request";

export const getters = {
}
export const actions = {
    async addContact({ commit }, opts) {
        return await axios.post('/contact/addContact', opts);
    },
}