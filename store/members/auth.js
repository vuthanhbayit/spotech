import axios from "~/services/request";

export const state = () => ({
})
export const mutations = {
}
export const getters = {
}
export const actions = {
    async loginSocial({ commit }, opts) {
        return await axios.post('/loginSocial', opts);
    },
    async checkSocialId({ commit }, opts) {
        return await axios.post('/checkSocialId', opts);
    },
    async loginEmail({ commit }, opts) {
        return await axios.post('/loginEmail', opts);
    },
    async registerEmail({ commit }, opts) {
        return await axios.post('/registerEmail', opts);
    },
    async verifyEmail({ commit }, opts) {
        return await axios.get('/verifyEmail', {
            params: opts
        });
    },
    async forgotPassword({ commit }, opts) {
        return await axios.post('/forgotPassword', opts);
    },
    async verifyTokenResetPassword({ commit }, opts) {
        return await axios.get('/verifyTokenResetPassword', {
            params: opts
        });
    },
    async changePassword({ commit }, opts) {
        return await axios.post('/changePassword', opts);
    },
}
