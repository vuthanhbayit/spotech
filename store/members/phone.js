import axios from "~/services/request";

export const state = () => ({
})
export const mutations = {
}
export const getters = {
}
export const actions = {
    async removePhone({ commit }, opts) {
        return await axios.post('/members/removePhone', opts);
    },
    async verifyPhone({ commit }, opts) {
        return await axios.post('/members/verifyPhone', opts);
    },
    async sendCodeVerifyPhone({ commit }, opts) {
        return await axios.post('/members/sendCodeVerifyPhone', opts);
    },
}
