import axios from "~/services/request";

export const actions = {

  //Basic
  async saveExpertBasic({commit}, opt) {
    return await axios.post('/members/saveExpertBasic', opt);
  },
  async detailExpertBasic({commit}, opts) {
    return await axios.get('/members/detailExpertBasic', {
      params: opts
    });
  },

  //Advanced
  async saveExpert({commit}, opt) {
    return await axios.post('/members/saveExpert', opt);
  },
  async detailExpert({commit}, opts) {
    return await axios.get('/members/detailExpert', {
      params: opts
    });
  },

  //update avatar, cover
  async updateAvatarCover({commit}, opts) {
    return await axios.post('/members/update-avatar-cover', opts);
  },
}
