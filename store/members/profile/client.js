import axios from "~/services/request";

export const actions = {
    //Basic
    async saveClientBasic({commit}, opt) {
        return await axios.post('/members/saveClientBasic', opt);
    },
    async detailClientBasic({ commit }, opts) {
        return await axios.post('/members/detailClientBasic', opts);
    },
    //Advanced
    async saveClient({commit}, opt) {
        return await axios.post('/members/saveClient', opt);
    },
    async detailClient({ commit }, opts) {
        return await axios.get('/members/detailClient', {
            params: opts
        });
    },
}
