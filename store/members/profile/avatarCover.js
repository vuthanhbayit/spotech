import axios from "~/services/request";

export const state = () => ({
})
export const mutations = {
}
export const getters = {
}
export const actions = {
    async saveAvatarCover({commit}, opt) {
        return await axios.post('/members/saveAvatarCover', opt);
    },
}
