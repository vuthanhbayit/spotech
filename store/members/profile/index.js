import axios from "~/services/request";
export const state = () => ({
    memberType: false,
})
export const mutations = {
    CHANGE_MEMBER_TYPE(state, payload) {
        state.memberType = payload
    },
}
export const getters = {
    memberType: state => state.memberType,
}
export const actions = {
    async changeMemberType({ commit }, opt) {
        commit('CHANGE_MEMBER_TYPE', opt)
    },
}
