import axios from "~/services/request";

export const actions = {
    async getShowProfile({}, opts) {
        return await axios.get('/members/getShowProfile', {
            params: opts
        });
    },
    async getCoverProfile({}, opts) {
        return await axios.get('/members/getCoverProfile', {
            params: opts
        });
    },
    async getExpertTranslation({}, opts) {
        return await axios.get('/members/getExpertTranslation', {
            params: opts
        });
    },
}
