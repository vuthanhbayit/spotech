import axios from "~/services/request";

export const state = () => ({
    search: []
})
export const mutations = {
    SEARCH_MEMBER(state, payload) {
        state.search = payload.data
    },
    RESET_SEARCH_MEMBER(state){
        state.search = [];
    }
}
export const getters = {
    search: state => state.search
}
export const actions = {
    async search ({commit}, opt) {
        const promise = await axios.post('/members/searchMembers', {...opt});
        commit('SEARCH_MEMBER', promise);
        return promise;
    },

    async resetSearch ({commit}) {
        commit('RESET_SEARCH_MEMBER');
    },

    async searchCallback ({commit}, {params, callback} ) {
        const promise = await axios.post('/members/searchMembers', params);
        commit('SEARCH_MEMBER', promise);
        if( callback ){ callback(null, promise); }
        return promise;
    },

    async saveWishListObject({commit}, opt) {
        return await axios.post('/members/saveWishListObject', opt);
    },
}
