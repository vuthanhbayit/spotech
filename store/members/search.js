import axios from "~/services/request";

export const state = () => ({
})
export const mutations = {
}
export const getters = {
}
export const actions = {
    async searchMembers({ commit }, opts) {
        return await axios.post('/members/searchMembers', opts);
    },
}
