import axios from "~/services/request";

export const state = () => ({
})
export const mutations = {
}
export const getters = {
}
export const actions = {

    //client
    async getShowProfile({ commit }, opts) {
        return await axios.get('/members/getShowProfile', {
            params: opts
        });
    },

    //expert
    async getExpertTranslation({ commit }, opts) {
        return await axios.get('/members/getExpertTranslation', {
            params: opts
        });
    },
}
