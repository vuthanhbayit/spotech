import axios from "~/services/request";

export const state = () => ({
})
export const mutations = {
}
export const getters = {
}
export const actions = {
    async followObject({ commit }, opts) {
        return await axios.post('/members/followObject', opts);
    },
    async getFollows({ commit }, opts) {
        return await axios.get('/members/getListFollowsByMember', opts);
    },
}
