import axios from "~/services/request";

export const actions = {
    async inviteCompany({commit}, opt) {
        return await axios.post('/members/inviteCompany', opt);
    },
    async joinCompany({commit}, opt) {
        return await axios.post('/members/joinCompany', opt);
    },
    async checkTokenJoinCompany({commit}, opt) {
        return await axios.post('/members/checkTokenJoinCompany', opt);
    },
    async listRoleCompany({commit}, opt) {
        return await axios.post('/members/listRoleCompany', opt);
    },
    async listUserByCompany({commit}, opt) {
        return await axios.post('/members/listUserByCompany', opt);
    },
    async changeRoleUserCompany({commit}, opt) {
        return await axios.post('/members/changeRoleUserCompany', opt);
    },
    async deleteUserCompany({commit}, opt) {
        return await axios.post('/members/deleteUserCompany', opt);
    }
}
