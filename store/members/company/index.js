import axios from "~/services/request";

export const state = () => ({
  companies: [],
})
export const mutations = {
  SET_COMPANIES(state, payload) {
    state.companies = payload
  }
}
export const getters = {
  companies: state => state.companies,
}
export const actions = {
  async listCompanyByMember({commit}, opt) {
    const {data} = await axios.post('/members/listCompanyByMember', opt);
    commit('SET_COMPANIES', data);
  },
}
