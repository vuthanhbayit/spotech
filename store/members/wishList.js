import axios from "~/services/request";
import {get_wish_list_folder } from '../../services/resource';

export const state = () => ({
    wishlist:[]
})
export const mutations = {
    SET_WISH_LIST(state, wishlist){
        state.wishlist = wishlist
    }
}
export const getters = {
}
export const actions = {
    async saveWishListFolder({ commit }, opts) {
        return await axios.post('/members/saveWishListFolder', opts);
    },
    async getListWishListFolder({ commit }, opts) {
        return await axios.post('/members/getListWishListFolder', opts);
    },
    async delWishListFolder({ commit }, opts) {
        return await axios.post('/members/delWishListFolder', opts);
    },
    async inviteLinkWishListFolder({ commit }, opts) {
        return await axios.post('/members/inviteLinkWishListFolder', opts);
    },
    async joinWishListFolder({ commit }, opts) {
        return await axios.post('/members/joinWishListFolder', opts);
    },
    async saveWishListObject({ commit }, opts) {
        return await axios.post('/members/saveWishListObject', opts);
    },
    async viewWishListFolder({ commit }, opts) {
        return await axios.post('/members/viewWishListFolder', opts);
    },
    getWishList(context, param){
        return new Promise((resolve, reject) => {
            get_wish_list_folder(param).then(response => {
                const wishlist = response.data
                context.commit('SET_WISH_LIST', wishlist)
                return resolve(response.data);
            }).catch(error => {
                reject(error);
            })
        })
    }
}
