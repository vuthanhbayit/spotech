import axios from "~/services/request";

export const actions = {
    async getAllContract({ commit }, opts) {
        return await axios.get('/contracts', {
            params: opts
        });
    },
    async storeContract({ commit }, opts) {
        return await axios.post('/contracts', opts);
    },
    async acceptContract({ commit }, opts) {
        return await axios.post(`/contracts/${opts.id}/accept`, opts);
    },

    async getAllNda({ commit }, opts) {
        return await axios.get('/ndas', {
            params: opts
        });
    },
    async storeNda({ commit }, opts) {
        return await axios.post('/ndas', opts);
    },
    async acceptNda({ commit }, opts) {
        return await axios.post(`/ndas/${opts.id}/accept`, opts);
    }
}