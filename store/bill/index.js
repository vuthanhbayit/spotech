import axios from "~/services/request";

export const actions = {
    
    async getAllTransactions({ commit }, params) {
        return await axios.post('/payment/getAllTransactions', params);
    },
    async getTransactionsByCustomer({ commit }, opts) {
        return await axios.post('/payment/getTransactionsByCustomer', opts);
    }
}
