import axios from "~/services/request";

export const state = () => ({})
export const mutations = {}
export const getters = {}
export const actions = {
    async addSurveyAi({ commit }, opts) {
        return await axios.post('/survey_ai', opts);
    },
    
}
