import axios from "~/services/request";

export const state = () => ({
})
export const mutations = {
}
export const getters = {
}
export const actions = {
    async deleteBlogCategory({ commit }, opts) {
        return await axios.post('/blogPost/deleteBlogCategory', opts);
    },
    async searchBlogCategory({ commit }, opts) {
        return await axios.post('/blogPost/searchBlogCategory', opts);
    },
    async saveBlogCategory({ commit }, opts) {
        return await axios.post('/blogPost/saveBlogCategory', opts);
    },
    async detailBlogCategory({ commit }, opts) {
        return await axios.post('/blogPost/detailBlogCategory', opts);
    },
}
