import axios from "~/services/request";

export const state = () => ({
})
export const mutations = {
}
export const getters = {
}
export const actions = {
    async deleteBlogPost({ commit }, opts) {
        return await axios.post('/blogPost/deleteBlogPost', opts);
    },
    async searchBlogPost({ commit }, opts) {
        return await axios.post('/blogPost/searchBlogPost', opts);
    },
    async saveBlogPost({ commit }, opts) {
        return await axios.post('/blogPost/saveBlogPost', opts);
    },
    async detailBlogPost({ commit }, opts) {
        return await axios.get('/blogPost/detailBlogPost', {
            params: opts
        });
    },
    async getListBlogPost({ commit }, opts) {
        return await axios.post('/blogPost/getListBlogPost', opts);
    },
}
