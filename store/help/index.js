import axios from "~/services/request";

export const state = () => ({})
export const mutations = {}
export const getters = {}
export const actions = {
    async searchListHelp({ commit }, opts) {
        return await axios.post('/help/searchHelp', opts);
    },
    async getHelpDetail({ commit }, opts) {
        return await axios.get('/help/detailHelp', {
            params: opts
        });
    },
}
