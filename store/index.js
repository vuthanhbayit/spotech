import {
  forgotPassword,
  login,
  register,
  socialLogin,
  verifyTokenResetPassword,
  changePassword,
  get_location,
  verifyEmailApi,
  updateUserProfile
} from '../services/auth';
import axios from "~/services/request";
import {setUserInfor, setCompany, setReferCode} from '~/plugins/information';
import config from '~/config';
import Cookie from 'js-cookie'
import _ from 'lodash';

export const state = () => ({
  locales: process.env.locales,
  locale: null,
  currency: null,
  auth: {},
  register: {},
  location: {},
  lastMessage: null,
  unit: null,
  company: null,
  referCode: null,
})

export const mutations = {
  SET_LANG(state, locale) {
    state.locale = locale
  },
  SET_CURRENCY(state, payload) {
    state.currency = payload
    Cookie.set(config.currencyKey, payload)
  },

  SET_CURRENCY_UNIT(state, unit) {
    state.unit = unit
  },
  SET_REGISTER(state, token) {
    let auth = {
      access_token: token.access_token || null,
      authUser: token || null
    };
    state.register = auth;
  },

  SET_TOKEN(state, token) {
    let auth = {
      access_token: token.access_token || null,
      authUser: token || null
    };
    state.auth = auth;
  },

  SET_LOCATION(state, location) {
    state.location = location
  },

  SET_LAST_MESSAGE_EVENT(state, event) {
    state.lastMessage = event;
  },
  SET_COMPANY(state, company) {
    state.company = company;
  },
  UPDATE_USER_PROFILE(state, profile) {
    if( state.auth && state.auth.authUser && state.auth.authUser.user) {
      state.auth.authUser.user = _.merge(state.auth.authUser.user, profile);
    }
  },
  SET_REFER_CODE(state, code) {
    state.referCode = code;
  }
}

export const getters = {
  loggedIn(state) {
    return state.auth.access_token != null
  },
  accessToken(state) {
    return state.auth != null && state.auth.access_token != null ? state.auth.access_token : null
  },
  referCode: state => state.referCode,
  getLocale: state => state.i18n && state.i18n.locale ? state.i18n.locale : '',
  getCurrency: state => state.currency,
  getCurrencyUnit: state => state.unit,
  getCompany: state => state.company,
  getUserId(state) {
    return state.auth != null && state.auth.authUser != null && state.auth.authUser.user != null && state.auth.authUser.user.id != null && state.auth.authUser.user.id > 0 ? state.auth.authUser.user.id : 0
  },
  getUserEmail(state) {
    return state.auth != null && state.auth.authUser != null && state.auth.authUser.user != null && state.auth.authUser.user.email != null && state.auth.authUser.user.email.length > 0 ? state.auth.authUser.user.email : null
  },
  getUserAvatar(state) {
    return state.auth != null && state.auth.authUser != null && state.auth.authUser.user && state.auth.authUser.user.avatar != null && state.auth.authUser.user.avatar.length > 0 ? state.auth.authUser.user.avatar : null
  },
  getUserFirstName(state) {
    return state.auth != null && state.auth.authUser != null && state.auth.authUser.user != null && state.auth.authUser.user.first_name != null && state.auth.authUser.user.first_name.length > 0 ? state.auth.authUser.user.first_name : null
  },
  getUserLastName(state) {
    return state.auth != null && state.auth.authUser != null && state.auth.authUser.user != null && state.auth.authUser.user.last_name != null && state.auth.authUser.user.last_name.length > 0 ? state.auth.authUser.user.last_name : null
  },
  getUserFullName(state) {
    let arr = [];
    if (state.auth != null && state.auth.authUser != null && state.auth.authUser.user != null && state.auth.authUser.user.first_name != null && state.auth.authUser.user.first_name.length > 0) {
      arr.push(state.auth.authUser.user.first_name);
    }
    if (state.auth != null && state.auth.authUser != null && state.auth.authUser.user != null && state.auth.authUser.user.last_name != null && state.auth.authUser.user.last_name.length > 0) {
      arr.push(state.auth.authUser.user.last_name);
    }
    if (arr.length > 0) {
      return arr.join(' ');
    }
    return '';
  },
  getCurrencyCode(state) {
    return state.auth != null && state.auth.authUser != null && state.auth.authUser.currency_code != null && state.auth.authUser.currency_code.length > 0 ? state.auth.authUser.currency_code : 'JPY'
  },
  isAuthenticated(state) {
    return state.auth != null && state.auth.authUser != null && state.auth.authUser.user != null && state.auth.authUser.user.id > 0 ? true : false
  }
}

export const actions = {
  async nuxtServerInit({ commit }, { req, app }) {
    
    if (req && req.session && req.session.register) {
      commit('SET_REGISTER', req.session.register)
      req.session.register = null;
    }
    if (req && req.session && req.session.user) {
      commit('SET_TOKEN', req.session.user)
    }
    
  },
  updateUserProfile(context, profile) {
    context.commit('UPDATE_USER_PROFILE', profile);
    context.dispatch('updateUserInfomation', profile);
  },
  updateUserInfomation(context, profile) {
    return updateUserProfile(profile);
  },
  getLocation(context) {
    return new Promise((resolve, reject) => {
      get_location().then(response => {
        const location = response.data
        context.commit('SET_LOCATION', location)
        return resolve(response.data);
      }).catch(error => {
        reject(error);
      })
    })
  },
  setDefaultCompany({commit}, company) {
    return new Promise((resolve, reject) => {
      setCompany(JSON.stringify(company));
      commit('SET_COMPANY', company);
      resolve({success: true});
    })
  },
  setReferCode({commit}, code) {
    return new Promise((resolve, reject) => {
      setReferCode(code);
      commit('SET_REFER_CODE', code);
      resolve({success: true});
    })
  },
  getUserInfor({commit}) {
    return new Promise((resolve, reject) => {
      axios.get('/members/getInfoMember').then(res => {
        setUserInfor(JSON.stringify(res.data));
        resolve({mess: 'Success'});
      })
        .catch(err => {
          reject(err);
        });
    })
  },
  login({commit}, data) {
    return new Promise((resolve, reject) => {
      login({
        ...data
      }).then((response) => {
        if (response != null && response.success == true) {
          const token = response.data
          commit('SET_TOKEN', token);
          return resolve(response.data);
        }
        return reject(response);
      }).catch(error => {
        console.log(error)
        reject(error)
      })
    });
  },


  loginSocial({commit}, data) {
    return new Promise((resolve, reject) => {
      socialLogin({
        ...data
      }).then((response) => {
        if (response != null && response.success == true) {
          const token = response.data
          commit('SET_TOKEN', token);
          return resolve(response.data);
        }
        return reject(response);
      }).catch(error => {
        reject(error)
      })
    });
  },

  register({commit}, data) {
    return new Promise((resolve, reject) => {
      register({
        ...data
      }).then((response) => {
        const token = response.data
        commit('SET_TOKEN', token);
        return resolve(response);
      }).catch(error => {
        reject(error)
      })
    });
  },

  verifyEmail({commit}, data) {
    return new Promise((resolve, reject) => {
      verifyEmailApi({
        ...data
      }).then((response) => {
        return resolve(response);
      }).catch(error => {
        reject(error)
      })
    });
  },

  forgotPassword({commit}, data) {
    return new Promise((resolve, reject) => {
      forgotPassword({
        ...data
      }).then((response) => {
        return resolve(response.data);
      }).catch(error => {
        reject(error)
      })
    });
  },

  async verifyTokenResetPassword({commit}, data) {
    return await verifyTokenResetPassword({...data})
  },

  async changePassword({commit}, data) {
    return await changePassword({...data})
  },
}
export const strict = false
