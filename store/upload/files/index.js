import axios from "~/services/request";

export const state = () => ({
})
export const mutations = {
}
export const getters = {
}
export const actions = {
    async upload({ commit }, opt) {
        return await axios.post('/files/uploadFiles', { ...opt });
    },
    async delete({ commit }, opt) {
        return await axios.post('/files/delete', { ...opt });
    },
}
