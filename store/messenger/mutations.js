import _ from 'lodash';
export default {
    SEARCH_DISCUSSION(state, payload) {
        state.discussions = payload.data;
    },
    SEARCH_DISCUSSION_TRASH(state, payload) {
        state.discussionTrashs = payload.data;
    },
    SEARCH_MEMBER(state, payload) {
        state.members = payload.data;
    },
    RESET_SEARCH_MEMBER(state) {
        state.members = [];
    },
    SEARCH_NOTIFICATION(state, payload){
        state.notifications = payload.data;
    },
    onPresenceChannelInitEvent(state, params) {
        let channelId = params.channelId;
        if( channelId ) {
            state.presences[channelId] = params.channel;
        }
    },
    onPresenceChannelJoin(state,params) {
        let channelId = params.channelId;
        if( channelId ) {
            state.presences[channelId] = params.channel;
        }
    },
    onPresenceChannelUsersJoin(state, params) {
        let channelId = params.channelId;
        if( channelId ) {
            state.presences[channelId][users] = params.users;
        }
    },
    onPresenceChannelUserJoin(state, params) {
        let channelId = params.channelId;
        if( channelId ) {
            state.presences[channelId].users.push( params.user );
        }
    },
    onPresenceChannelUserLeave(state, params) {
        let channelId = params.channelId;
        if( channelId ) {
            let users = state.presences[channelId].users;
            let user = params.user;
            let index = _.findIndex(users, (u) => {
                return u.id == user.id;
            })
            if( index >= 0 ) {
                users.splice(index, 1);
                state.presences[channelId].users = users;
            }
        }
    },
    onPresenceChannelDiscussionEvent(state, params) {
        // current do not use yet
    },
    onPresenceChannelMessageEvent(state, params) {
        let channelId = params.channelId;

        if( channelId )
        {
            let message = params.event.message;
            if( message != null )
            {
                let messages = state.presences[channelId].messages;

                let index = _.findIndex(messages.items, function (m) {
                    return m.id == message.id
                });
                if (index >= 0) {
                    messages.items.splice(index, 1);
                }
                state.presences[channelId].messages = messages;
                if( params.callback ) {
                    let callback = params.callback;
                    callback(null, message);
                }
            }

        }
    }
}
