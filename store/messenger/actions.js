import {
    searchDisccusionByUser,
    searchDiscussionTrashByUser,
    removeDiscussion,
    forceDeleteDiscussion,
    forceDeleteDiscussionAll,
    searchMemberChat,
    markAsRead,
    markAsReadAll,
    searchNotificationByUser,
    markAsReadAllNotification,
    getDiscussion,
    sendMessage
} from '~/services/messenger'
import _ from 'lodash';

const messageDefault = {
    items: [],
    meta:{
        count: 0,
        current_page: 0,
        per_page: 10,
        next: 1,
        prev: null
    }
};

const conversationDefault = {
    page: 1,
    channel: null,
    channelId: null,
    conversionId: null,
    users: [],
    messages: _.cloneDeep( messageDefault ),
    isTrash: false,
    isReady: false
};

export default {
    async listenPresenceChannel (context, { channelId, callback, onMessageCallback }) {
        if( !context.state.presences[channelId] )
        {
            context.commit('onPresenceChannelInitEvent', { channelId: channelId, channel: _.clone(conversationDefault)} );
        }

        let channel =  Echo.join(channelId)
            .here( function(users) {
                context.commit('onPresenceChannelUsersJoin', {channelId: channelId, users: users} );
            })
            .joining( function(user) {
                //console.log('joining', user);
                context.commit('onPresenceChannelUserJoin', {channelId: channelId, user: user} );
            })
            .leaving( function(user) {
                context.commit('onPresenceChannelUserLeave', {channelId: channelId, user: user} );
            })
            .listen('.discussion', (e) => {
                context.commit('onPresenceChannelDiscussionEvent', {channelId: channelId, event: e} );
            })
            .listen('.message', (e) => {
                //console.log('event message: ', e)
                context.commit('onPresenceChannelMessageEvent', {channelId: channelId, event: e, callback: onMessageCallback} );
            })
            .listenForWhisper('typing', function(e) {
                context.commit('onPresenceChannelTypingEvent', {channelId: channelId, event: e} );
            })
            .notification( (e) => {
                context.commit('onPresenceChannelNotificationEvent', {channelId: channelId, event: e} );
            } );


        context.commit('onPresenceChannelJoin',{ channelId: channelId, channel: channel});
        if( callback ) { callback(null, { channelId: channelId, channel: channel }) }
    },
    async listenPrivateChannel (context, { channelId, callback } ) {
        let channel = Echo.private(channelId)
        .listen('.message', (e) => {
            content.commit('onPrivateChannelMessageEvent', e);
        });
        context.commit('onPrivateChannelJoin',{ channelId: channelId, channel: channel});
        if( callback ) { callback(null, { channelId: channelId, channel: channel }) }
    },
    /*async listenPublicChannel ({commit}, params) {

    },*/
    async searchMemberByUser ({commit}, params) {
        const promise = await searchMemberChat(params);
        commit('searchMemberChat', promise);
        return promise;
    },

    async searchMemberChat ({commit}, opt) {
        const promise = await searchMemberChat(opt);
        commit('SEARCH_MEMBER', promise);
        return promise;
    },

    async searchMemberChatCallback ({commit}, opt ) {
        const promise = await searchMemberChat(opt);
        return promise;
    },

    async resetSearchMember ({commit}) {
        commit('RESET_SEARCH_MEMBER');
    },

    async searchDiscussionByUser ({commit}, params) {
        const promise = await searchDisccusionByUser(params);
        commit('SEARCH_DISCUSSION', promise);
        return promise;
    },

    async searchDiscussionTrashByUser ({commit}, params) {
        const promise = await searchDiscussionTrashByUser(params);
        commit('SEARCH_DISCUSSION_TRASH', promise);
        return promise;
    },

    async searchDiscussionByUserCallback ({commit}, { params , callback } ) {
        const promise = await searchDisccusionByUser(params);
        commit('SEARCH_MEMBER', promise);
        if( callback ){ callback(null, promise); }
        return promise;
    },

    async markConversationTrash (context, params ) {
        const promise = await removeDiscussion(params);
        //commit('SEARCH_MEMBER', promise);
        //context.dispatch('searchByUser');

        //if( callback ){ callback(null, promise); }
        return promise;
    },

    async markConversationForceDelete (context, params ) {
        const promise = await forceDeleteDiscussion(params);
        //commit('SEARCH_MEMBER', promise);
        //context.dispatch('searchByUser');

        //if( callback ){ callback(null, promise); }
        return promise;
    },

    async markConversationForceDeleteAll (context, params ) {
        const promise = await forceDeleteDiscussionAll(params);
        //commit('SEARCH_MEMBER', promise);
        //context.dispatch('searchByUser');

        //if( callback ){ callback(null, promise); }
        return promise;
    },

    async markAsRead (context, { params , callback } ) {
        const promise = await markAsRead(params);
        //commit('SEARCH_MEMBER', promise);
        //context.dispatch('searchByUser');

        //if( callback ){ callback(null, promise); }
        return promise;
    },

    async markAsReadAll (context, { params , callback } ) {
        const promise = await markAsReadAll(params);
        //commit('SEARCH_MEMBER', promise);
        //context.dispatch('searchByUser');

        //if( callback ){ callback(null, promise); }
        return promise;
    },

    async searchNotificationByUser ({commit}, params) {
        const promise = await searchNotificationByUser(params);
        commit('SEARCH_NOTIFICATION', promise);
        return promise;
    },

    async markAsReadAllNotification (context, { params , callback } ) {
        const promise = await markAsReadAllNotification(params);
        //commit('SEARCH_MEMBER', promise);
        //context.dispatch('searchByUser');

        //if( callback ){ callback(null, promise); }
        return promise;
    },
    async getConversation (context, params ) {
        const promise = await getDiscussion(params);

        return promise;
    },

    async sendMsg(context, params) {
        const promise = await sendMessage(params);

        return promise;
    }

}
