import axios from "~/services/request";

export const state = () => ({
})
export const mutations = {
}
export const getters = {
}
export const actions = {
    async addRating({ commit }, opts) {
        return await axios.post('/rating/addRating', opts);
    },
    async getRatingOption({ commit }, opts) {
        return await axios.get('/rating/getRatingOption', {
            params: opts
        });
    },
    async getRatingOptionValue({ commit }, opts) {
        return await axios.get('/rating/getRatingOptionValue', {
            params: opts
        });
    },

    async addReview(context, opts) {
        return await axios.post(`/statistic/review/${opts.type}/create`, opts);
    },

    async getReviewList(context, opts) {
        return await axios.get(`/statistic/review/${opts.type}/${opts.objectId}/list`, { params: opts} );
    },

    async getReviewSummary(context, opts) {
        return await axios.get(`/statistic/review/${opts.type}/${opts.objectId}/summary`, { params: opts} );
    },
}
