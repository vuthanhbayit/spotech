import axios from "~/services/request";

export const state = () => ({
})
export const mutations = {
}
export const getters = {
}
export const actions = {
    async saveTicket({ commit }, opts) {
        return await axios.post('/ticket/saveTicket', opts);
    },
    async addTicketReply({ commit }, opts) {
        return await axios.post('/ticket/addTicketReply', opts);
    },
    async getTickets({ commit }, opts) {
        return await axios.get('/ticket/getTickets', {
            params: opts
        });
    },
    async getTicketTranslation({ commit }, opts) {
        return await axios.get('/ticket/getTicketTranslation', {
            params: opts
        });
    },
    async getTicketReplyTranslation({ commit }, opts) {
        return await axios.get('/ticket/getTicketReplyTranslation', {
            params: opts
        });
    },
}
