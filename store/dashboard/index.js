import axios from "~/services/request";

export const actions = {
    async saveClocks({ commit }, opts) {
        return await axios.post('/statistic/saveClocks', opts);
    },
    async getClocks({ commit }, opts) {
        return await axios.get('/statistic/getClocks', {
            params: opts
        });
    },
    async readReceiveBid({ commit }, opts) {
        return await axios.post('/statistic/readReceiveBid', opts);
    },
    async readReceiveAdjustTimeBid({ commit }, opts) {
        return await axios.post('/statistic/readReceiveAdjustTimeBid', opts);
    },
    async getOverviewProfile({ commit }, opts) {
        return await axios.get('/statistic/getOverviewProfile', {
            params: opts
        });
    },
    async getManageProject({ commit }, opts) {
        return await axios.get('/statistic/getManagerProject', {
            params: opts
        });
    },
    async getManageBlog({ commit }, opts) {
        return await axios.get('/statistic/getManagerBlog', {
            params: opts
        });
    },
    async saveAdjustTimeBid({ commit }, opts) {
        return await axios.post('/statistic/saveAdjustTimeBid', opts);
    },
    async acceptProgressBid({ commit }, opts) {
        return await axios.post('/statistic/acceptProgressBid', opts);
    },
    async updateAdjustTime({ commit }, opts) {
        return await axios.post('/statistic/updateAdjustTime', opts);
    },
    async updateExtensionAdjustTime({ commit }, opts) {
        return await axios.post('/statistic/updateExtensionAdjustTime', opts);
    },
    async updateObjectBidAccept({ commit }, opts) {
        return await axios.post('/statistic/updateObjectBidAccept', opts);
    },
    async updateObjectBidProgress({ commit }, opts) {
        return await axios.post('/statistic/updateObjectBidProgress', opts);
    },
    async getManagerPayment({ commit }, opts) {
        return await axios.get('/statistic/getManagerPayment', {
            params: opts
        });
    },
    async getPercent({ commit }, opts) {
        return await axios.get('/statistic/getPercent', { params: opts} );
    },
    async projectFinished({ commit }, opts) {
        return await axios.post('/statistic/project-finished', opts );
    },
    async getManagerNotification({ commit }, opts) {
        return await axios.post('/payment/sendNotifyPayment', opts);
    },
    async getAccountBillingInfo({ commit }, opts) {
        return await axios.post('/payment/getAccount', opts );
    },

    async addAccountBillingInfo({ commit }, opts) {
        return await axios.post('/payment/createAccount', opts );
    },

    async getMoneyFuture({ commit }, opts) {
        return await axios.post('/payment/getMoneyFuture', opts );
    },

    async updateAccountBillingInfo({ commit }, opts) {
        return await axios.post('/payment/updateAccount', opts );
    },
    
}
