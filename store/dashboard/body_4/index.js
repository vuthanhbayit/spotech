import axios from "~/services/request";

export const state = () => ({
})
export const mutations = {
}
export const getters = {
}
export const actions = {
    async getBidObjectReceive({ commit }, opts) {
        return await axios.get('/statistic/getBidObjectReceive', {
            params: opts
        });
    },
    async acceptProject({ commit }, opts) {
        return await axios.post('/statistic/acceptReceiveBid', opts);
    },
}
