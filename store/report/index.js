import axios from "~/services/request";

export const getters = {
}
export const actions = {
    async addReport({ commit }, opts) {
        return await axios.post('/report/addReport', opts);
    },
    async getReportOption({ commit }, opts) {
        return await axios.get('/report/getReportOption', {
            params: opts
        });
    },
}
