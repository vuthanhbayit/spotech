import axios from "~/services/request";

export const actions = {
    async searchProjects({ commit }, opts) {
        return await axios.post('/project/searchProjects', opts);
    },
    async getHomeProjects({ commit }, opts) {
        return await axios.post('/project/getHomeProjects', opts);
    },
    async getHomeExpert({ commit }, opts) {
        return await axios.post('/members/getHomeMembers', opts);
    },
}
