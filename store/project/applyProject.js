import axios from "~/services/request";

export const state = () => ({
})
export const mutations = {
}
export const getters = {
}
export const actions = {
    async saveBidProject({ commit }, opts) {
        return await axios.post('/project/saveBidProject', opts);
    },
    async getApplyProjects({ commit }, opts) {
        return await axios.get('/project/getBidProjects', {
            params: opts
        });
    },
    async getApplyProjectTranslation({ commit }, opts) {
        return await axios.get('/project/getBidProjects', {
            params: opts
        });
    },
}
