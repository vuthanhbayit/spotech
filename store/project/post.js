import axios from "~/services/request";

export const state = () => ({
})
export const mutations = {
}
export const getters = {
}
export const actions = {
    async saveProject({ commit }, opts) {
        return await axios.post('/project/saveProject', opts);
    },
    async getEditProject({ commit }, opts) {
        return await axios.get('/project/getEditProject', {
            params: opts
        });
    },
    async getShowProject({ commit }, opts) {
        return await axios.get('/project/getShowProject', {
            params: opts
        });
    },
    async getCoverProject({ commit }, opts) {
        return await axios.get('/members/getCompanyByProjectId', {
            params: opts
        });
    },
    async getProjectTranslation({ commit }, opts) {
        return await axios.get('/project/getProjectTranslation', {
            params: opts
        });
    },
    async copyProject({ commit }, opts) {
        return await axios.get('/project/copyProject', {
            params: opts
        });
    },
}
