import axios from "~/services/request";
export const actions = {
    async searchClient({ commit }, opt) {
        return await axios.post('/resource/nation/searchClient', { ...opt });
    },
    async searchResource({ commit }, opt) {
        return await axios.post('/resource/nation/searchResource', { ...opt });
    },
}
