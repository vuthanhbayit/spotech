import axios from "~/services/request";

export const actions = {
  async searchClient({commit}, opt) {
    return await axios.post('/language/searchClient', {...opt});
  },
  async searchResource({commit}, opt) {
    return await axios.post('/resource/language/searchResource', {...opt});
  },
  async getListLanguageStatic({commit}, opt) {
    return await axios.get('/resource/language/getListLanguageStatic', {
      params: opt
    });
  },
}
