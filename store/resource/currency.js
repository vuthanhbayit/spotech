import axios from "~/services/request";

export const actions = {
    async search({ commit }, opt) {
        return await axios.post('/resource/currency/search', opt);
    },
    async getCurrency({ commit }, opt) {
        return await axios.post('/resource/currency/getCurrency', opt);
    },
}
