import axios from "~/services/request";

export const state = () => ({
  languageClient: [],
  nationResource: [],
  nationClient: [],
  workEnvironment: [],
  category: [],
  duration: [],
  reviewConditions:{
    loaded: false,
    items: [],
    promise: null
  },
  clocks: [],
  ranking: [],
  nationStrip: []
})
export const mutations = {
  SET_RESOURCE(state, payload) {
    state.languageClient = payload.languageClient
    state.nationResource = payload.nationResource
    state.nationClient = payload.nationClient
    state.workEnvironment = payload.workEnvironment
    state.category = payload.category
    state.duration = payload.duration
    state.clocks = payload.clock
    state.ranking = payload.ranking
  },
  SET_REVIEW_CONDITION(state, payload) {
    state.reviewConditions.items = payload.data;
    state.reviewConditions.promise = payload.promise;
    state.reviewConditions.loaded = true;
  },
  SET_STRIP_NATION(state, payload) {
    state.nationStrip = payload
  }
}
export const getters = {
  languageClient: state => state.languageClient,
  nationResource: state => state.nationResource,
  workEnvironment: state => state.workEnvironment,
  category: state => state.category,
  duration: state => state.duration,
  nationClient: state => state.nationClient,
  categories5: state => state.category.slice(0, 5),
  nationClient5: state => state.nationClient.slice(0, 5)
}
export const actions = {
  async searchLanguageLevel({commit}, opt) {
    return await axios.post('/resource/languageLevel/search', {...opt});
  },
  async searchDuration({commit}, opt) {
    return await axios.post('/resource/duration/search', {...opt});
  },
  async searchRanking({commit}, opt) {
    return await axios.post('/resource/ranking/search', {...opt});
  },
  async searchSkill({commit}, opt) {
    return await axios.post('/resource/skill/search', {...opt});
  },
  async searchDegree({commit}, opt) {
    return await axios.post('/resource/degree/search', {...opt});
  },
  async searchMajor({commit}, opt) {
    return await axios.post('/resource/major/search', {...opt});
  },
  async searchUniversity({commit}, opt) {
    return await axios.post('/resource/university/search', {...opt});
  },
  async searchWorkEnvironment({commit}, opt) {
    return await axios.post('/resource/workEnvironment/search', {...opt});
  },
  async searchWorkWelfare({commit}, opt) {
    return await axios.post('/resource/workWelfare/search', {...opt});
  },
  async searchYearExperience({commit}, opt) {
    return await axios.post('/resource/yearExperience/search', {...opt});
  },
  async searchRoleId({commit}, opt) {
    return await axios.post('/resource/roleBid/search', {...opt});
  },
  async getWorkEnvironment({commit}, opt) {
    return await axios.post('/resource/workEnvironment/search', {...opt});
  },
  async getAllStripNation({commit}, opt) {
    let res = await axios.post('/resource/strip-nation', {...opt});
    if( res.success ) {
      commit('SET_STRIP_NATION', res.data)
    }
    return res;
  },
  
  async getAll({commit}, opt) {
    let promise = null;
    try{
      promise = JSON.parse(localStorage.getItem('resource') );
    }catch(e){}
    if( !promise ) {
      promise = await axios.post('/resource/getAllResource', {...opt});
      localStorage.setItem('resource', JSON.stringify(promise) );
    }
    commit('SET_RESOURCE', promise.data);
    return promise
  },
  async getReviewCondition({state, commit}) {
    if( state.reviewConditions && state.reviewConditions.loaded ){ return Promise.resolve( state.reviewConditions.promise ); }
    const promise = await axios.get('/statistic/review/condition/all');
    //console.log( {promise: promise, data: promise.data} );
    if( promise && promise.data && promise.data.success) {
      commit('SET_REVIEW_CONDITION', {promise: promise, data: promise.data} );
    }
    return promise
  },

  async translate({state, commit}, params) {
    const promise = await axios.post('/translate', params);
    return promise
  },
}
