
import configPro from './env/production';
import configDev from './env/development';
const env = process.env.NODE_ENV || 'development';
export default  env === 'development' ? configDev : configPro;
