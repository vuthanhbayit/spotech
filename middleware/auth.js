export default function ({ app, store, route, redirect }) {
  if ( ! store.getters.accessToken ) {
    return redirect(app.localePath({ name: 'index', query: { redirect_back: route.path } }));
  }else{
    if(process.browser){
      localStorage.setItem("LoginTypes", true);
    }
  }
}
