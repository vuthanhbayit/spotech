import config from '~/config'
import { loadTranslation } from '~/services/lang'

const getCookie = (cookiename, list) => {
  // Get name followed by anything except a semicolon
  var cookiestring=RegExp(""+cookiename+"[^;]+").exec(list);
  // Return everything after the equal sign, or an empty string if the cookie name not found
  return decodeURIComponent(!!cookiestring ? cookiestring.toString().replace(/^[^=]+./,"") : "");
}

const setAppLocale = async (app, req) => {

    let lang = config.defaultLocale;
    if( req ) {
        lang = getCookie(config.lang, req.headers.cookie);
    }
    if( app.$moment ) {
        app.$moment.locale = lang;
    }
    const messages = await loadTranslation(lang, '');
    
    await app.i18n.setLocale(lang);
    await app.i18n.setLocaleMessage(messages);
}

export default async function ({ app, req }) {
    //await setAppLocale(app, req);
}
