
const TwitterStrategy = require('passport-twitter-oauth2').Strategy;
const FacebookStrategy = require('passport-facebook').Strategy;
const LinkedinStrategy = require('@sokratis/passport-linkedin-oauth2').Strategy;
const LocalStrategy = require('passport-local').Strategy;
import secret from '../secret';


exports = module.exports = function(app, passport) {
    //local
    passport.use(
        new LocalStrategy(secret.local,
        function(username, password, cb) {
            return cb(null, {email: username, passowrd: password} )
        }),
    );
    // twitter
    passport.use(
        new TwitterStrategy(secret.twitter,
        function(accessToken, tokenSecret, profile, cb) {
            profile.access_token = accessToken;

            return cb(null, profile)
        }),
    );

    passport.use(new FacebookStrategy( secret.facebook,
        function(accessToken, refreshToken, profile, cb) {
            profile.access_token = accessToken;
            return cb(null, profile)
        }
    ));

    //link fields: https://developer.linkedin.com/docs/fields/basic-profile
    passport.use(new LinkedinStrategy(
        secret.linkedin,
        function(accessToken, tokenSecret, profile, cb) {
            profile.access_token = accessToken;
            return cb(null, profile)
        }
    ));

    passport.serializeUser((user, done) => {
        done(null, user);
    });

    passport.deserializeUser((user, done) => {
        done(null, user);
    });

    return passport;
}
