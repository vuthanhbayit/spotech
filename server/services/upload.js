const aws = require('aws-sdk');
const multer = require('multer');
const multerS3 = require('multer-s3');
//const fileType = require('file-type');
import secret from '../../secret';

aws.config.update(secret.aws);
const s3 = new aws.S3();

const fileFilter = (req, file, cb) => {
    cb(null, true);
    /*if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png')
    {
        cb(null, true);
    }
    else
    {
        cb(new Error('Invalid file type, only JPEG and PNG is allowed!'), false);
    }*/
}

const upload = multer({
    fileFilter,
    storage: multerS3({
        acl: secret.aws.acl,
        s3,
        bucket: secret.aws.bucket,
        metadata: function (req, file, cb)
        {
            cb(null, {fieldName: file.fieldname});
        },
        key: function (req, file, cb) {
            var ext = file.originalname.substring(file.originalname.lastIndexOf('.')-1, file.originalname.length);
            cb(null, 'uploads/'+Date.now().toString()+ext)
        }
    })
});

module.exports = upload;
