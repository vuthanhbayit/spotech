const express = require('express')
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')
const passport = require('passport');
const session = require('express-session');
import secret from '../secret';
import config from '../config';
import axios from 'axios';


const app = express();
app.enable('trust proxy');
app.use(cookieParser());
/*
if( process.env.NODE_ENV == 'production')
{
    const redis = require('redis');
    const redisClient = redis.createClient(secret.redis);
    let redisStore = require('connect-redis')(session);
    secret.session.store = new redisStore({ client: redisClient, ttl: secret.redis.ttl });
}
*/
app.use(session(secret.session));
app.use(bodyParser.json());
app.use(require('express-useragent').express());
app.use(passport.initialize());
app.use(passport.session());
require('./passport')(app, passport);
const auth = require('./routes/auth')(passport);
const upload = require('./routes/upload')();

app.use((req, res, next) => {
    const sessionUser = req.session.user;
    let requestToken = req.header('Authorization');
	if ((sessionUser && sessionUser.access_token) ) {
        let token = requestToken || sessionUser.access_token;
        if( token ) {
            if( token.indexOf('Bearer') < 0 ) {
                token = `Bearer ${token}`;
            }
            axios.defaults.headers.common['Authorization'] = token;
        }
    }
    let locale = config.defaultLocale;
    let lang = req.header('Language-Code');
    if( lang ) {
        locale = lang;
    }
    axios.defaults.headers.common['Language-Code'] = locale;
    axios.defaults.headers.common['X-Request-Time'] = (new Date()).getTime();
    axios.defaults.headers.common['Content-Type'] = 'application/json; charset=utf-8';
    axios.defaults.headers.common['X-Requested-Env'] = 'web';
    axios.defaults.headers.common['Accept'] = "application/json";  
    next();
});

app.use(auth);
app.use(upload);

module.exports = {
    path: '/app',
    prefix: false,
    handler: app
}
