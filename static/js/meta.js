let vp = document.getElementById('viewport');
let bp = window.matchMedia(' (max-width: 767px) ');
const changeMedia = function (bp) {
    if ( !bp.matches ) {
        vp.setAttribute('content', 'width=1280, user-scalable=no');
    }
}
changeMedia(bp);