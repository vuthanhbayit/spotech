import config from './config';
import secret from './secret';
import CopyWebpackPlugin from 'copy-webpack-plugin'
import CompressionPlugin from 'compression-webpack-plugin'
import PurgecssPlugin from 'purgecss-webpack-plugin'
import glob from 'glob-all'
import path from 'path'

module.exports = {

  //mode: 'spa',

  server: config.app.server,
  /*
	** Headers of the page
	*/
  head: config.app.head,
  /*
	** Customize the progress bar color
	*/
  // loading: '~/components/share/loading.vue',
  loading: {
    color: '#fff',
    height: '2px'
  },

  css: [
    '~/assets/scss/styles.scss',
    '~/assets/scss/styles.css',
    '@mdi/font/css/materialdesignicons.css',
    '@fortawesome/fontawesome-free/css/all.css',
    'element-ui/packages/theme-chalk/lib/icon.css',
    'element-ui/packages/theme-chalk/lib/index.css',
    'iview/dist/styles/iview.css',
    '~/assets/scss/customByHa.css',
    '~/assets/scss/customByHa_newDesign.css',
  ],
  /*
	** Build configuration
	*/
  build: {
    extractCSS: true,
    cssSourceMap: true,
    extend(config, { isDev, isClient }) {
      if (!isDev && isClient) {
        config.plugins.push(
          new PurgecssPlugin({
            paths: glob.sync([
              path.join(__dirname, './**/**/*.vue'),
            ]),
            whitelist: ['html', 'body']
          })
        )
      }
    },
    plugins: [
      new CopyWebpackPlugin([
        {from: 'node_modules/tinymce/skins', to: 'skins'}
      ]),
      new CompressionPlugin()
    ]
  },
  plugins: [
    {src: '~/plugins/i18n', mode: 'client'},
    '~/plugins/axios',
    {src: '~/plugins/laravel-echo', mode: 'client'},
    // {src: '~/plugins/init', mode: 'client'},
    {src: '~/plugins/elementUI', ssr: true},
    {src: '~/plugins/iview', mode: 'client'},
    '~/plugins/helper',
    {src: '~/plugins/vuelidate', mode: 'client'},
    {src: '~/plugins/directives', mode: 'client'},
    {src: '~/plugins/notify', mode: 'client'},
    '~/plugins/memberType',
    {src: '~/plugins/sharing', mode: 'client'},
    {src: '~/plugins/methods', mode: 'client'},
    {src: '~/assets/js/app', mode: 'client'},
  ],
  buildModules: [
    '@nuxtjs/moment'
  ],

  modules: [
    'nuxt-polyfill',
    'cookie-universal-nuxt',
    //'@nuxtjs/onesignal',
    '@nuxtjs/sentry',
    '@nuxtjs/proxy',
    '@nuxtjs/pwa',
    [
      'nuxt-i18n',
      {
        locales: [
          {code: 'en', iso: 'en-US', file: 'en.js'},
          {code: 'ja', iso: 'ja-JP', file: 'ja.js'},
          {code: 'vi', iso: 'vi-VN', file: 'vi.js'}
        ],
        differentDomains: false,
        defaultLocale: config.defaultLocale,
        lazy: true,
        langDir: 'locales/',
        //detectBrowserLanguage: false,
        detectBrowserLanguage: {
          // If enabled, a cookie is set once a user has been redirected to his
          // preferred language to prevent subsequent redirections
          // Set to false to redirect every time
          useCookie: true,
          // Cookie name
          cookieKey:  config.i18n.cookieName,
          // Set to always redirect to value stored in the cookie, not just once
          alwaysRedirect: true,
          // If no locale for the browsers locale is a match, use this one as a fallback
          fallbackLocale: config.defaultLocale
        },
        vuex: {
            // Module namespace
            moduleName: 'i18n',

            // If enabled, current app's locale is synced with nuxt-i18n store module
            syncLocale: true,

            // If enabled, current translation messages are synced with nuxt-i18n store module
            syncMessages: true,

            // Mutation to commit to set route parameters translations
            syncRouteParams: true
         },
      }
    ],
    [
      'nuxt-session',
      (session) => {
          if( process.env.NODE_ENV == 'production')
          {
              const redis = require('redis');
              const redisClient = redis.createClient(secret.redis);
              let redisStore = require('connect-redis')(session);
              secret.session.store = new redisStore({ client: redisClient, ttl: secret.redis.ttl })
          }

          return secret.session;
      }
    ],
  ],
  //oneSignal: config.oneSignal,
  sentry: config.sentry,
  vue: config.vue,
  proxy:{
      '/api/client': config.serverApiHost,
      '/_conversations': config.serverApiHost,
      //'/app': config.hostApiUrl,
  },
  polyfill:{
    features: [{
        require: 'filereader-polyfill'
    },{
        require: 'notification-polyfill'
    },{
      require: 'url-polyfill'
    }]
  },
  router: {
    middleware: ['locale']
  },
  serverMiddleware:[
    '~/server/index.js',
    //'~/server/locale.js',
  ],
  env: {
    environment: process.env.NODE_ENV
  }
}
