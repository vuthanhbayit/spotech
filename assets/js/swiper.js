import Swiper from 'swiper';

// slide-partner
$(document).ready(function () {
    var swiper = new Swiper('.slide-partner', {
        slidesPerView: 5,
        loop: true,
        autoplay: {
            delay: 2500,
            disableOnInteraction: false,
        },
        breakpoints: {
            320: {
                slidesPerView: 1,
            },
            567: {
                slidesPerView: 1,
            },
            767: {
                slidesPerView: 2,
            },
            991: {
                slidesPerView: 3,
            },
        }
    });


    // slide-item-prd
    var swiperprd = new Swiper('.slide-item-prd', {
        slidesPerView: 1,
        navigation: {
            nextEl: '.btn-next',
            prevEl: '.btn-prev',
        },
    });



});