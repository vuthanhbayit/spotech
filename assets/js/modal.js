$(document).ready(function () {
    $('[data-toggle="modal"]').on('click', function () {
        let target = $(this).attr('data-target');
        $('.modal').modal('hide');
        $(target).modal('show');
    });
        
});
