import service from '../request';

export function getCompanies() {
  return service.post('/members/listCompanyByMember');
}

export function getCompanyDetail(queryString) {
  return service.get('/members/detailClient', { params: queryString });
}

export function getCoverProfile(queryString) {
  return service.get('/members/getCoverProfile', { params: queryString });
}

export function getCompanyProfile(queryString) {
  return service.get('/members/getShowProfile', { params: queryString});
}

export function getMembers(params) {
  return service.post('/members/listUserByCompany', params);
}

export function addNewMember(params) {
  return service.post('/members/inviteCompany', params);
}

export function joinCompany(params) {
  return service.post('/members/joinCompany', params);
}

export function checkTokenJoinCompany(params) {
  return service.post('/members/checkTokenJoinCompany', params);
}

export function getProjects(queryString) {
  return service.get('/members/getProjectsByCompany', { params: queryString});
}

export function updateStatus(params) {
  return service.post('/updateProfileProjectStatus', params);
}

export function changeRoleMember(params) {
  return service.post('/company/change-role-member', params);
}

export function deleteMember(params) {
  return service.post('/company/delete-member', params);
}
