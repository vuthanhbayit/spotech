import service from '~/services/request';

export function savePayment(params) {
  return service.post('/payment/savePayment', params);
}

export function getIntent() {
  return service.get('/payment/getIntent');
}

export function getCards(params) {
  return service.post('/payment/getAllPaymentMethod', params);
}

export function addPaymentMethod(params) {
  return service.post('/payment/addPaymentMethod', params);
}

export function removePaymentMethod(params) {
  return service.post('/payment/removePaymentMethod', params);
}

export function paymentForSpotech(params) {
  return service.post('/payment/payment', params);
}

