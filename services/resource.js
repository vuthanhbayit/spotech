import axios from './request';

export function categorySearch( params ) {
    return axios.post( `/categories/search`,  params );
}
export function nationSearch( params ) {
    return axios.post('/resource/nation/searchClient',  params );
}
export function workEnvironmentSearch( params ) {
    return axios.post('/resource/workEnvironment/search',  params );
}
export function getProfileMember( params ) {
    return axios.get('/members/detailExpert');
}
export function getListNation(){
    return axios.post('/resource/nation/searchResource');
}
export function verifyPhone(params){
    return axios.post('/members/sendCodeVerifyPhone', params );
}
export function sendCodeVerify(params){
    return axios.post('/members/verifyPhone', params );
}
export function deletePhone(params){
    return axios.post('/members/removePhone',params );
}
export function getLanguage(){
    return axios.post('/language/searchClient' );
}
export function get_list_category(){
    return axios.post('/categories/search');
}
export function get_list_skill(params){
    return axios.post('/resource/skill/search', params);
}
export function get_year_experience(){
    return axios.post('/resource/yearExperience/search');
}
export function get_list_language(params){
    return axios.post('/resource/language/searchResource',params);
}
export function get_language_level(){
    return axios.post('/resource/languageLevel/search');
}
export function get_list_degree(){
    return axios.post('/resource/degree/search');
}
export function get_list_maijor(params){
    return axios.post('/resource/major/search',params);
}
export function get_list_univesity(params){
    return axios.post('/resource/university/search',params);
}
export function get_list_currency(params){
    return axios.post('/resource/currency/search',params);
}
export function get_list_duration(params){
    return axios.post('/resource/duration/search',params);
}
export function save_expert(params){
    return axios.post('/members/saveExpert',params);
}
export function save_create_project(params){
    return axios.post('/project/saveProject',params);
}
export function get_wish_list_folder(params){
    return axios.post('/members/getListWishListFolder',params);
}
export function get_work_environment(params){
    return axios.post('/resource/workEnvironment/search',params);
}
