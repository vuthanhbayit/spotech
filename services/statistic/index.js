import service from '~/services/request';

export function saveClock(params) {
  return service.post('/statistic/saveClocks', params);
}
