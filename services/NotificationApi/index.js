import service from '../request'

export function getNotifications(query) {
  return service.get('/notification/getNotificationGroups', {
    params: query
  });
}

export function updateNotifications(params) {
    return service.post('/notification/saveNotifications', params);
}
