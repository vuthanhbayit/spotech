import service from '~/services/request';

export function getWishListFolder(opts) {
  return service.post('/members/getListWishListFolder', opts);
}
