import Vue from 'vue'
import http from 'axios';
import config from '../config'

const reqMethods = [
  'request', 'delete', 'get', 'head', 'options', // url, config
  'post', 'put', 'patch' // url, data, config
];
let axios = {}

reqMethods.forEach((method) => {
  axios[method] = async function () {

   arguments[0] = ( process.client ? window.location.origin : config.serverApiHost ) +
                      ( (arguments[0].indexOf('_conversations') == -1 && arguments[0].indexOf('app') == -1 ) ? '/api/client' : '') +
                      arguments[0];
    //arguments[0] = !arguments[0].path ? (config.hostApiUrl + (arguments[0].indexOf('_conversations') == -1 ? '/api/client' : '') + arguments[0]) : arguments[0].path;
    //console.log(arguments)
    const unAuthenticateStatus = 401;
    try {
      const promise = await http[method].apply(null, arguments)
      if (promise.data.success) return promise.data
      throw promise.data
    } catch (e) {
      console.log('api request e', arguments, e, http.defaults.headers.common)
      if (e.message && e.message.indexOf(unAuthenticateStatus) >= 0 && process.client) {
        return _handleRequestUnauthentication();
      }
      throw e
    }
  }
})

const _handleRequestUnauthentication = () => {
  const path = window.location.pathname;
  window.location.href = `/app/logout?redirect_back=${path}`;
}

export default axios
