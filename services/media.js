import service from './request'

export function upload(params) {
    return service.post('/images/upload',params);
}

export function remove(params) {
    return service.delete('/images/delete',params);
}

export function uploadFile(params) {
    return service.post('/app/files/uploadFiles', params);
}

export function removeFile(params) {
    return service.post('/app/files/delete',params);
}
