import service from './request';
import _ from 'lodash';

export function removeDiscussion( params ) {
    return service.delete( `/_conversations/discussions/${params.discussionId}/trash`,  params );
}

export function forceDeleteDiscussion( params ) {
    return service.delete( `/_conversations/discussions/${params.discussionId}/delete`,  params );
}

export function forceDeleteDiscussionAll( params ) {
    return service.delete( `/_conversations/search/by_user/trash/remove`,  params );
}

export function getDiscussionMessages( params ) {

    return service.get( `/_conversations/discussions/${params.discussionId}/messages`, {
        params: params
    } );
}
export function countMessageUnread( params ) {

    return service.get( `/_conversations/search/by_user/un_read/count`,  params );
}

export function searchMemberChat( params ) {
    return service.post( '/members/searchMembersChat', { ...params } );
}

export function searchDisccusionByUser( params ) {
    return service.post( `/_conversations/search/by_user`,  params );
}

export function searchDiscussionTrashByUser( params ) {
    return service.post( `/_conversations/search/by_user/trash`,  params );
}

export function sendMessage( params ) {
    return service.post( `/_conversations/discussions/${params.discussionId}/messages`,  params );
}

export function sendFile( params ) {
    return service.post( `/_conversations/discussions/${params.discussionId}/messages`,  params );
}

export function searchDisccusionAttach( params ) {
    return service.get( `/_conversations/discussions/${params.discussionId}/attachments`,  {
        params: params
    } );
}

export function markAsRead( params ) {
    return service.post( `/_conversations/discussions/${params.discussionId}/mark_as_read`,  {
        params: params
    } );
}

export function markAsReadAll( params ) {
    return service.post( `/_conversations/discussions/mark_as_read_all`,  {
        params: params
    } );
}

export function searchNotificationByUser( params ) {
    return service.post( `/_conversations/notifications/search/by_user`,  params );
}

export function markAsReadAllNotification( params ) {
    return service.post( `/_conversations/notifications/mark_as_read_all`,  {
        params: params
    } );
}

export function getNotificationCount( params ) {
    return service.get(`/_conversations/notifications/search/by_user/un_read/count`, {
        params: params
    } );
}

export function getMessageCount( params ) {
    return service.get(`/_conversations/search/by_user/un_read/count`, {
        params: params
    } );
}

// get conversion
export function parseType(arrSelect) {
    arrSelect = _.uniqBy(arrSelect, 'id');
    arrSelect = _.sortBy(arrSelect,  (o)=> o.type).reverse();
    let disccusion = {
        type: -1,
        ids: []
    };

    if (! arrSelect || arrSelect.length <= 0 ) {
      return disccusion;
    }

    if( arrSelect.length == 1 )
    {
        let elem = arrSelect[0];
        disccusion.ids = [elem.id];
        if( elem.type == 1 )
        {
            //me to other member
            disccusion.type = 0;
            disccusion.memberId = elem.id;
            disccusion.ids = [elem.id ];
        }
        if( elem.type == 2 )
        {
            //me to company
            disccusion.type = 2;
            disccusion.companyId = elem.id;
            disccusion.ids = [elem.id ];
        }
    }

    if( arrSelect.length > 1 )
    {
        let elem = arrSelect[0];

        if( elem.type == 1 )
        {
            //me to others
            arrSelect.forEach( ( select ) => {
                if( select.type == 1 ) {
                    disccusion.ids.push( select.id );
                }
            } );
            if( disccusion.ids.length > 1 ){ disccusion.type = 1; }
        }
        else if( elem.type == 2 )
        {
            //company to company
            let second = arrSelect[1];
            if( second.type == 1 ) {
                disccusion.type = 4;
                disccusion.companyId = elem.id;
                disccusion.userId = second.id;
            }
            else {
                disccusion.type = 3;
                disccusion.firstCompanyId = elem.id;
                disccusion.secondCompanyId = second.id;
            }
            disccusion.ids = [elem.id, second.id];
        }
        else
        {
            arrSelect.forEach( ( select ) => {
                disccusion.ids.push( select.id );
            } );
        }
    }

    return disccusion;
}

export function getDiscussion( arrSelect, conversation = null ) {
    let disccusion = parseType(arrSelect);
    if( conversation ) {
        disccusion = _.merge(disccusion, conversation);
    }
    switch( disccusion.type )
    {
        case 0:
            return getDiscussionMeVsOther( disccusion );
        case 1:
            return getDiscussionMeVsOthers( disccusion );
        case 2:
            return getDiscussionMeVsCompany( disccusion );
        case 3:
            return getDiscussionCompanyVsCompany( disccusion );
        case 4:
            return getDiscussionUserVsCompany(disccusion);
        default:
            return new Promise( ( resolve, reject ) => { reject({message: 'Type invalid', type:'notSupportChatMethod'}); } );
            //return service.post( '/_conversations/discussions/get_or_create',  arrSelect );
    }
}

export function updateDiscussion( conversation, arrSelect = null) {
    //let disccusion = parseType(arrSelect);
    if( arrSelect && arrSelect.length > 0 )
    {
        conversation.members = arrSelect;
    }
    return service.put( `/_conversations/discussions/${conversation.id}`,  conversation );
}

export function updateBasicDiscussion( conversation ) {
    return service.put( `/_conversations/discussions/${conversation.id}/update`,  conversation );
}

export function getDiscussionMeVsOther( params ) {
    return service.post( `/_conversations/discussions/me/vs/member/${params.memberId}/get_or_create`,  params );
}

export function getDiscussionMeVsOthers( params ) {
    return service.post( `/_conversations/discussions/me/vs/members/get_or_create`,  params );
}

export function getDiscussionMeVsCompany( params ) {
    return service.post( `/_conversations/discussions/me/vs/company/${params.companyId}/get_or_create`,  params );
}

export function getDiscussionUserVsCompany( params ) {
    return service.post( `/_conversations/discussions/user/${params.userId}/vs/company/${params.companyId}/get_or_create`,  params );
}

export function getDiscussionCompanyVsCompany( params ) {
    return service.post( `/_conversations/discussions/company/${params.firstCompanyId}/vs/company/${params.secondCompanyId}/get_or_create`,  params );
}
