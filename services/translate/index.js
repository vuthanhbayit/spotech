import service from '~/services/request';

export function translateContentBid(queryString) {
  return service.get('/project/getBidProjectTranslation', { params: queryString });
}
