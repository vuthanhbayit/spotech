import axios from './request';
import config from '../config';

export function login( params ) {
    return axios.post('/app/login', params );
}

export function register( params ) {
    return axios.post('/app/register',  params );
}

export function verifyEmail( params ) {
    return axios.post('/app/verifyEmail',  params );
}

export function socialLogin( params ) {
    return axios.post('/app/loginSocial',  params );
}

export function updateUserProfile( params ) {
    return axios.post({ path: config.baseUrl + '/app/profile/update' },  params );
}

export function loginApi( params , header) {
    return axios.post('/loginEmail', params, {
        headers: header
    } );
}

export function logoutApi( params , header) {
    return axios.post('/logout', params, {
        headers: header
    } );
}

export function verifyEmailApi( params, header = null ) {
    let args = {
        params: {
            ...params
        },
    };
    if( header ) {
        args.headers = header;
    }
    return axios.get( '/verifyEmail', args);
}

export function registerApi( params, header = null ) {
    return axios.post('/registerEmail',  params , {
        headers: header
    });
}

export function socialLoginApi( params, header = null ) {
    return axios.post('/loginSocial',  params, {
        headers: header
    } );
}

export function checkSocialIdApi( params, header = null ) {
    return axios.post('/checkSocialId',  params , {
        headers: header
    });
}

export function forgotPassword( params ) {
    return axios.post('/forgotPassword',  params );
}

export function verifyTokenResetPassword( params ) {
    return axios.get('/verifyTokenResetPassword', {
        params: {
            ...params
        }
    });
}

export function changePassword( params ) {
    return axios.post('/changePassword',  params );
}
export function get_location( params ) {
    return axios.get('/getLocationMember');
}

export function verifySocial(params, header) {
    return axios.post('/members/verifySocial', params, {
        headers: header
    });
}
