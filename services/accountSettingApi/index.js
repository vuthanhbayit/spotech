import service from '../request'

export function changePassword(params) {
    return service.post('/members/changePasswordBySetting', params);
}


export function getLoginLog(params) {
  return service.get('/oauth/logs', {
    params: params
  });
}

export function revokeToken(params) {
  return service.post('/oauth/logs/'+params.id+'/revoke', params);
}

export function revokeTokenAll(params) {
  return service.post('/oauth/logs/'+params.id+'/revoke/all', params);
}
