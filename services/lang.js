import axios from './request'

export async function loadTranslation(lang, currency) {
  let key = 'lang'.concat(lang);
  let promise = null;
  try {
    promise = JSON.parse(localStorage.getItem(key));
  } catch (e) {
  }
  if (process.server) {
    return Promise.resolve();
  }
  if (!promise) {
    promise = await axios.get(`/resource/language/getListLanguageStatic`, {
      headers: {
        'Language-Code': lang,
        'X-Requested-Env': 'web',
        'Currency-Code': currency,
      }
    });
  }
  return Promise.resolve(promise.data);
}

export async function getAllResource(store) {
  if (process.server) {
    return Promise.resolve();
  }

  const promise = await store.dispatch('resource/getAll')
  return Promise.resolve(promise.data);
}
