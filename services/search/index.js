import service from '~/services/request';

export function search(params) {
  return service.post('/search-mix', params);
}

export function getDurations() {
  return service.post('/resource/duration/search');
}
