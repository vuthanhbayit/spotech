import service from '../request';

export function followOrUnFollow(params) {
    return service.post('/members/followObject', params);
}
