import {assignWith} from 'lodash';
import {loadTranslation, getAllResource} from '../services/lang';

const useDefault = (value, defaultValue) =>
  typeof value === 'object' ?
    assignWith({}, value, defaultValue, useDefault) :
    value || defaultValue;

export default async ({store}) => await Promise.all([
  loadTranslation('vi', 'VND'),
  getAllResource(store)
])
  .then(([defaultMessages, localizedMessages]) =>
    assignWith({}, localizedMessages, defaultMessages, useDefault));
